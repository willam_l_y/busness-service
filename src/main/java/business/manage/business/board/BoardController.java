package business.manage.business.board;

import business.manage.business.issue.Issue;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/businessManage")
@Api(value = "看板")
@CrossOrigin
public class BoardController {

    private final BoardService boardService;

    public BoardController(BoardService borderService) {
        this.boardService = borderService;
    }

    //todo 这个接口是统计型的接口
    @GetMapping("/boardInfo")
    public Board boardInfo() {
        return boardService.boardInfo();
    }


    @GetMapping("/boardInfoMock")
    public Board boardInfoMock() {
        Board board = new Board();
        board.setClientAreaDistribution(
                Map.of("北京", 10L, "上海", 20L, "广东", 30L, "河南", 40L, "湖北", 50L));
        board.setProjectCount(10);
        board.setCustomerCount(20);
        board.setNewThisMonth(30);
        board.setNegotiateProjects(40);
        board.setBidProjects(50);
        board.setOfferProjects(60);
        board.setSignedProjects(70);
        board.setClientTypeDistribution(
                Map.of("主机厂", new ClientTag2Count(10, 20L),
                        "零部件厂", new ClientTag2Count(20, 30L),
                        "集成商", new ClientTag2Count(30, 40L),
                        "设备商", new ClientTag2Count(40, 50L),
                        "平台组织", new ClientTag2Count(50, 60L),
                        "科研院所", new ClientTag2Count(60, 70L),
                        "高校", new ClientTag2Count(70, 80L),
                        "其他", new ClientTag2Count(80, 90L)
                ));
        board.setProjectProportion(
                Map.of(0, 5L, 1, 10L, 2, 20L, 3, 30L, 4, 40L, 5, 50L, 6, 60L, 7, 70L, 8, 80L, 9, 90L));
        board.setIssues(List.of(
                new Issue(0, 1722351709000L, "技术方案实施过程中发现不能达到预期效果", "进行中", "未解决", null, null),
                new Issue(1, 1722351709000L, "系统测试不通过", "进行中", "未解决", null, null),
                new Issue(2, 1722351709000L, "成本超过预算", "进行中", "已解决", null, null),
                new Issue(3, 1722351709000L, "项目进度不及预期", "进行中", "已解决", null, null)
        ));
        board.setIntentions(
                Map.of(
                        0, Map.of(0, 10L, 1, 20L, 2, 30L, 3, 40L, 4, 50L),
                        1, Map.of(0, 10L, 1, 20L, 2, 30L, 3, 40L, 4, 50L),
                        2, Map.of(0, 10L, 1, 20L, 2, 30L, 3, 40L, 4, 50L)
                )
        );
        return board;
    }
}
