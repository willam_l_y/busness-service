package business.manage.business.docking_people;

import business.manage.business.client.Client;
import business.manage.business.common.CommonResponse;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
//@SpringBootTest
public class ClientDockingPeopleTest {

    @Autowired
    ClientDockingPeopleRepository dockingPeopleRepository;
    @Autowired
    ClientDockingPeopleController dockingPeopleController;

    @BeforeEach
    public void setup() {
    }

    //    @Test
    public void testCreateClientDockingPeople() {


    }

    //测试ClientDockingPeopleTest的ClientDockingPeopleController
//    @Test
    public void testClientDockingPeopleController() {
//        ResponseEntity<CommonResponse<List<ClientDockingPeople>>> clientDockingPeopleList =  dockingPeopleController.get("John");
//        clientDockingPeopleList.getBody().getData().forEach(data->
//            //如何获取forEach的参数？
//          log.error(data.toString())
//        );
    }


    /**
     * 测试ClientDockingPeopleController的get方法
     */
    @Test
    public void testAll() {
        var a = new HashSet<>(Set.of("1", "2"));
        var b = new HashSet<>(Set.of("2", "3"));
        var copyA = new HashSet<>(a);
        copyA.removeAll(b);
        var copyB = new HashSet<>(b);
        copyB.removeAll(a);
        log.error(copyA.toString());
        log.error(copyB.toString());
        // 获取A比B少了那个元素
        //var d = a.retainAll(b);
      //  log.error(a.toString());
    }
}