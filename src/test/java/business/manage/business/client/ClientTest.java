package business.manage.business.client;


import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

//@SpringBootTest
public class ClientTest {
    private static final Logger log = LoggerFactory.getLogger(ClientTest.class);
//    @Autowired
    private ClientRepository clientRepository;


    public void testCreate() {
        Client res = clientRepository.getOne(1);
        System.out.println(res);
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    }

//    @Test
    public void testFindById() {
        Optional<Client> res = clientRepository.findById(3);
        var value = res.get();
        System.out.println(value);
        var docking = value.getClientDockingPeople();
        log.error(docking.toString());
        docking.forEach(
                clientDockingPeople -> System.out.println(clientDockingPeople)
        );
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    }
    /**
     * 测试:findByClientIdAndRequirementType
     */
//    @Test
    public void testFindByClientIdAndRequirementType(){
        var res = clientRepository.findByClientIdAndRequirementType(1, 1);
        res.forEach(
                client -> System.out.println(client)
        );
    }

//    @Test
//    public void testFindRequirementTypesByClientId() {
//        var res = clientRepository.findRequirementTypesByClientId(1);
//        System.out.println(res);
//    }
}
