package business.manage.business.docking_people;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClientDockingPeopleRepository extends JpaRepository<ClientDockingPeople,Integer> {
   // 实现findByName方法
    Page<ClientDockingPeople> findByNameContaining(String name,Pageable pageable);
    Page<ClientDockingPeople> findAll(Pageable pageable);
}
