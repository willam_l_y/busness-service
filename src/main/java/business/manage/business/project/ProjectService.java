package business.manage.business.project;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static business.manage.business.Tools.commonDataFormatter;
@Slf4j
@Service
public class ProjectService {
    private final ProjectRepository projectRepository;

    @Autowired
    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public List<Project> findProjectsByMilepostsBetweenAndType(String month, Optional<Integer> type) {
        month = month.trim();
        LocalDate start = getFirstDayOfMonth(month);
        LocalDate end = getLastDayOfMonth(month);
        return projectRepository.findProjectsFiltrateByMilepostsMonthAndType(start, end, type);
    }

    //实现一个方法，传入一个月份，返回这个月份的第一天的日期
    // 比如传入2020-01，返回2020-01-01
    public LocalDate getFirstDayOfMonth(String month) {
        return LocalDate.parse(month + "-01", commonDataFormatter);
    }

    //实现一个方法，传入一个月份，返回这个月份的最后一天的日期
    // 比如传入2020-01，返回2020-01-31
    public LocalDate getLastDayOfMonth(String month) {
        return getFirstDayOfMonth(month).plusMonths(1);
    }
}
