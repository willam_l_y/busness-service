package business.manage.business.exchange;

import business.manage.business.milepost.Milepost;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * 交流详情
 */
@Data
@Entity
@ApiModel(description = "交流详情")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Exchange {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ApiModelProperty("我方人员输出内容")
    private String ourPersonnelOutputContent;
    @ApiModelProperty("客户反馈内容")
    private String customerFeedbackContent;
    @ApiModelProperty("场外互动情况")
    private String offSiteInteractions;
    @ApiModelProperty("目前困滩点")
    private String difficultyPoints;
    @ApiModelProperty("合作点")
    private String cooperationPoints;
    @ApiModelProperty("分析机会点")
    private String opportunityPoints;
    @ApiModelProperty("下一步跟进计划")
    private String programContent;
    @ApiModelProperty("需要哪些部门支持")
    private String departmentalSupport;

    @Min(0)
    @Max(9)
    @NotNull
    @ApiModelProperty("需求/对接类别。 0:轻量化智能产线、1:智能产线、2:技术服务、3:产品、4:软件、5:数字化、6:试制、7:测试、8:其他、9:工装")
    private Integer requirementType;

    @Min(0)
    @Max(6)
    @NotNull
    @ApiModelProperty("当前进度. 0:待接触,1:洽谈、2:报价、3:投标、4:签单、5:暂停、6:终止")
    private Integer progress;

    @Pattern(regexp =  "^[ABCDE]$", message = "客户评级只能为A、B、C、D、E")
    @ApiModelProperty("客户评级. A：重要、B：大型、C：一般、D：徘徊、E：无意向。")
    private String customerRatings;

    @Min(0)
    @Max(4)
    @ApiModelProperty("合作意向。 0：强烈，1：明确，2：待挖掘，3：徘徊，4：无意向")
    private int cooperationIntentions;
    @ApiModelProperty("项目编号")
    private String projectNo;
    @ApiModelProperty("订单金额")
    private String orderAmount;
    @ApiModelProperty("签单日期")
    private String signingDate;
    @ApiModelProperty("填写日期")
    private String fillInDate;
    @ApiModelProperty("我方对接人")
    private String ourDockingPeople;

    @Min(0)
    @Max(2)
    @ApiModelProperty("我方对接人所属公司。0:数格、1：莱特威特、2：装备")
    private int ourGroupOfDockingPeople;


    @ApiModelProperty("里程碑")
    @ManyToOne
    @JsonIgnore
    private Milepost milepost;
}
