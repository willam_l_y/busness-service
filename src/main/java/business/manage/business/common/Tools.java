package business.manage.business.common;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class Tools {
    /**
     * 生成通用的返回对象
     * @param resData  响应中的data对象
     * @return ResponseEntity<CommonResponse<T>>
     * @param <T> 响应中的data对象类型
     */
    public static <T> ResponseEntity<CommonResponse<T>> genCommonResponseResponseEntity(T resData) {
        CommonResponse<T> response = new CommonResponse<>(HttpStatus.OK.value(), "success", resData);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
