package business.manage.business.ability;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface AbilityRepository extends JpaRepository<Ability,Integer> {
    // 添加逻辑删除的方法
    @Transactional
    @Modifying
    @Query("UPDATE Ability a SET a.isDeleted = true WHERE a.id = :id")
    void logicalDelete(@Param("id") Integer id);

    // 添加逻辑恢复的方法
    @Transactional
    @Modifying
    @Query("UPDATE Ability a SET a.isDeleted = false WHERE a.id = :id")
    void logicalUndelete(@Param("id") Integer id);

    // 修改查询方法，使其只返回未被逻辑删除的记录
    @Query("SELECT a FROM Ability a WHERE a.isDeleted = false")
    List<Ability> findAllNotDeleted();
}
