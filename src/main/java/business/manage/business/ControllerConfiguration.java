package business.manage.business;


import business.manage.business.common.CommonResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.constraints.Null;
import java.util.HashMap;
import java.util.Map;


@ControllerAdvice
public class ControllerConfiguration {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<CommonResponse<Null>> internalServerError(Exception e) {
        e.printStackTrace();
        CommonResponse<Null> response = new CommonResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage(), null);
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ResponseEntity<CommonResponse<Null>> methodArgumentNotValid(MethodArgumentNotValidException e) {
        e.printStackTrace();
        Map<String, String> errors = new HashMap<>();
        e.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        CommonResponse<Null> response = new CommonResponse<>(HttpStatus.BAD_REQUEST.value(), errors.toString(), null);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}
