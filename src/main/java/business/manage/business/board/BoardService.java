package business.manage.business.board;

import business.manage.business.client.ClientRepository;
import business.manage.business.exchange.ExchangeService;
import business.manage.business.issue.Issue;
import business.manage.business.issue.IssueRepository;
import business.manage.business.project.ProjectRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class BoardService {
    private final ClientRepository clientRepository;
    private final ProjectRepository projectRepository;
    private final ExchangeService exchangeService;
    private final IssueRepository issueRepository;
    //类型(主机厂：0、零部件厂：1、集成商：2、设备商：3、平台组织：4、科研院所：5、高校：6、其他：7）
    Map<Integer, String> clientType2Name = Map.of(
            0, "主机厂",
            1, "零部件厂",
            2, "集成商",
            3, "设备商",
            4, "平台组织",
            5, "科研院所",
            6, "高校",
            7, "其他"
    );

    public BoardService(ClientRepository clientRepository,
                        ProjectRepository projectRepository,
                        ExchangeService exchangeService,
                        IssueRepository issueRepository
    ) {
        this.clientRepository = clientRepository;
        this.projectRepository = projectRepository;
        this.exchangeService = exchangeService;
        this.issueRepository = issueRepository;
    }

    public Board boardInfo() {
        Board board = new Board();
        //项目概况的数据

        board.setProjectCount(projectRepository.findAll().size());
        board.setCustomerCount(clientRepository.findAll().size());
        //todo 本月新增的项目数量
        board.setNewThisMonth(0);
        //todo 项目进度是洽谈状态(Project.progress == 1)的项目的数量
        board.setNegotiateProjects(exchangeService.countByProgress(1));
        //todo 项目进度是投标状态(Project.progress == 3)的项目的数量
        board.setBidProjects(exchangeService.countByProgress(3));
        //todo 项目进度是报价状态(Project.progress == 2)的项目的数量
        board.setOfferProjects(exchangeService.countByProgress(2));
        //todo 项目进度是签单状态(Project.progress == 4)的项目的数量
        board.setSignedProjects(exchangeService.countByProgress(4));
        //查询客户类型分布
        board.setClientTypeDistribution(typeDistribution());
        //合作意向
        board.setIntent0count(exchangeService.countOurGroupOfDockingPeople(0));
        board.setIntent1count(exchangeService.countOurGroupOfDockingPeople(1));
        board.setIntent2count(exchangeService.countOurGroupOfDockingPeople(2));
        board.setIntentions(cooperationIntentions());
        //项目概况的数据
        //交流详情的需求/对接类别的数据
        board.setProjectProportion(exchangeService.findRequirementTypeCounts());
        board.setIssues(issues());
        return board;
    }

    /**
     * 获取类型分布
     * @return <类型的名称，<类型tag，数量>>
     */
    public Map<String, ClientTag2Count> typeDistribution() {
        List<Object[]> typeCounts = clientRepository.findClientTypeCounts();
        return typeCounts.stream().map(typeCount -> {
            int typeKey = (int) typeCount[0];
            long typeValue = (long) typeCount[1];
            return Map.entry(
                    clientType2Name.get(typeKey),
                    new ClientTag2Count(typeKey, typeValue)
            );
        }).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }
    /**
     * 合作意向分析结果
     *
     * @return
     */
    public Map<Integer, Map<Integer, Long>> cooperationIntentions() {
        //todo 从数据库中查询合作意向分析的数据
        return exchangeService.findOurGroupOfDockingPeopleCountsAndGroupByCooperationIntentions();
    }

    /**
     * 问题列表
     */
    public List<Issue> issues() {
        return issueRepository.findAll();
    }
}
