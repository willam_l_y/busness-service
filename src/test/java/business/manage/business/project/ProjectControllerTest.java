package business.manage.business.project;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;

//@SpringBootTest
public class ProjectControllerTest {

    // ... existing tests ...
//    @Autowired
    ProjectService service;

//    @Test

    public void testSearch() throws Exception {
        // Step 3: Define the input parameters for the search method
        int clientId = 1;
        int type = 0;
        String month = "2024-10";
        var res = service.findProjectsByMilepostsBetweenAndType(month, Optional.of(clientId));
        System.out.println(res);
        //断言res变量中的数据在一条或者以上
        //assertTrue(res.size() >= 1);
    }
}