package business.manage.business;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;


public class Tools {
    /**
     * 判断一个日期是否在本周
     *
     * @param timeStamp 日期
     * @return boolean 是否在本周
     */
    public static boolean isTimeInThisWeek(long timeStamp) {
        LocalDate localDate = longToLocalDate(timeStamp);
        LocalDate thisWeekMonday = LocalDate.now().with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
        LocalDate thisWeekSunday = LocalDate.now().with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY));
        return localDate.isAfter(thisWeekMonday) && localDate.isBefore(thisWeekSunday);
    }


    public static LocalDate longToLocalDate(long timeStamp) {
        return Instant.ofEpochMilli(timeStamp).atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static DateTimeFormatter commonDataFormatter =DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static LocalDate comDateParser(String date){
        return LocalDate.parse(date, commonDataFormatter);
    }
}
