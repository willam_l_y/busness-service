package business.manage.business.docking_people;

import business.manage.business.client.Client;
import business.manage.business.project.Project;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.*;
import javax.validation.constraints.*;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Data
@Entity
@ToString(exclude = "client projects")
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ClientDockingPeople {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @ApiModelProperty("姓名")
    @Column(unique = true)
    @NotBlank(message = "客户对接人的姓名为必填")
    private String name;

    @ApiModelProperty("一级部门")
    private String departmentLevel1;
    @ApiModelProperty("二级部门")
    private String departmentLevel2;
    @ApiModelProperty("职位")
    private String position;
    @ApiModelProperty("年龄")
    private int age;

    @Min(0)
    @Max(1)
    @ApiModelProperty("性别")
    private Integer gender;
    @ApiModelProperty("籍贯")
    private String nativePlace;
    @ApiModelProperty("家庭住址")
    private String address;
    @ApiModelProperty("毕业学校")
    private String graduateSchool;
    @ApiModelProperty("子女情况")
    private String childrenSituation;
    @ApiModelProperty("婚姻状况")
    private String maritalStatus;
    @ApiModelProperty("工作经历")
    private String workExperience;
    @ApiModelProperty("客户关系圈")
    private String relationship;

    @Pattern(regexp = "^1[3-9]\\d{9}$|^(\\(\\d{3,4}\\)|\\d{3,4}-|\\s)?\\d{7,14}$", message = "电话号码格式不正确")
    @ApiModelProperty("联系电话")
    private String phone;

    @Pattern(regexp = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$", message = "邮箱格式不正确")
    @ApiModelProperty("邮箱")
    private String mail;

    @ManyToMany(mappedBy = "clientDockingPeople", fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<Client> clients;

    @ManyToOne
    @ApiModelProperty("项目")
    @JsonIgnore
    private Project projects;
    public ClientDockingPeople() {

    }
}