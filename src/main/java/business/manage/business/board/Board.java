package business.manage.business.board;

import business.manage.business.issue.Issue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@ApiModel(description = "看板")
public class Board {
    // 项目概况
    /**
     * 项目总数
     */
    @ApiModelProperty("项目总数")
    private int projectCount;
    /**
     * 客户总数
     */
    @ApiModelProperty("客户总数")
    private int customerCount;
    /**
     * 本月新增
     */
    @ApiModelProperty("本月新增")
    private int newThisMonth;
    /**
     * 洽谈项目
     */
    @ApiModelProperty("洽谈项目")
    private int negotiateProjects;
    /**
     * 投标项目
     */
    @ApiModelProperty("投标项目")
    private int bidProjects;
    /**
     * 报价项目
     */
    @ApiModelProperty("报价项目")
    private int offerProjects;
    /**
     * 签单项目
     */
    @ApiModelProperty("签单项目")
    private int signedProjects;
    /**
     * 客户类型分布
     */
    @ApiModelProperty("客户类型分布")
    private Map<String , ClientTag2Count> clientTypeDistribution;
    /**
     * 项目占比
     */
    @ApiModelProperty("项目占比")
    private Map<Integer,Long> projectProportion;

    //意向分析
    /**
     * 数格意向个数
     */
    @ApiModelProperty("数格意向个数")
    private int Intent0count;
    /**
     * 来特斯特意向个数
     */
    @ApiModelProperty("来特斯特意向个数")
    private int Intent1count;
    /**
     * 装备意向个数
     */
    @ApiModelProperty("装备意向个数")
    private int Intent2count;
    /**
     * 意向分析
     */
    @ApiModelProperty("意向分析")
    private Map<Integer, Map<Integer, Long>> intentions;
    //客户地区分布
    /**
     * 客户地区分布
     */
    @ApiModelProperty("客户地区分布")
    private Map<String, Long> clientAreaDistribution;
    //本周问题表
    /**
     * 本周问题表
     */
    @ApiModelProperty("本周问题表")
    private List<Issue> issues;

}



@Data
class ClientTag2Count {
    @ApiModelProperty("客户类型的tag")
    Long tag;
    @ApiModelProperty("客户类型的数量")
    Long count;
    public ClientTag2Count(long tag, Long count) {
        this.tag = tag;
        this.count = count;
    }
}

