package business.manage.business.board;

import business.manage.business.client.ClientRepository;
import business.manage.business.exchange.ExchangeRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

//@SpringBootTest
public class BoardTest {
//    @Autowired
    private ClientRepository clientRepository;
//    @Autowired
    private BoardService boardService;
//    @Autowired
    private ExchangeRepository repository;

//    @Test
    public void testFindClientTypeCounts() {
        //todo 从数据库中查询客户类型分布的数据
        //todo 以Map的形式存储到board对象中
        List<Object[]> res = clientRepository.findClientTypeCounts();
        for (Object[] re : res) {
            System.out.println("类型：" + re[0] + "===个数：" + re[1]);
        }
        System.out.println("================");
    }

//    @Test
    public void setClientService() {
        var info = boardService.typeDistribution();
        System.out.println(info);
    }

    //测试ExchangeRepository的findOurGroupOfDockingPeopleCountsAndGroupByCooperationIntentions
//    @Test
    public void testFindOurGroupOfDockingPeopleCountsAndGroupByCooperationIntentions() {
        var res = repository.findOurGroupOfDockingPeopleCountsAndGroupByCooperationIntentions();
        for (Object[] re : res) {
            System.out.println("公司:" + re[0] + "===意向：" + re[1] + "===个数：" + re[2]);
        }
    }
    //测试boardService.boardInfo()
//    @Test
    public void testBoardInfo() {
        var res = boardService.boardInfo();
        System.out.println(res);
        assert res != null;
    }











}
