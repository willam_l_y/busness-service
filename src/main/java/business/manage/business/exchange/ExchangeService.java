package business.manage.business.exchange;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ExchangeService {

    private final ExchangeRepository repository;

    public ExchangeService(ExchangeRepository repository) {
        this.repository = repository;
    }

    //查出所有的Exchange的ourGroupOfDockingPeople属性值为1的个数
    public int countOurGroupOfDockingPeople(int ourGroupType) {
        return repository.countByOurGroupOfDockingPeople(ourGroupType);
    }

    /**
     * 查出Exchange各个ourGroupOfDockingPeople类型的数量并根据cooperationIntentions属性分组
     *
     * @return
     */
    public Map<Integer, Map<Integer, Long>> findOurGroupOfDockingPeopleCountsAndGroupByCooperationIntentions() {
        var result = repository.findOurGroupOfDockingPeopleCountsAndGroupByCooperationIntentions();
        //实现将result遍历，取出其元素，元素是一个数组，数组的第一个元素是公司，第二个元素是意向，第三个元素是个数
        //将公司存入Map的key，其value是一个子Map，子Map的key是意向，value是个数
        return result.stream().collect(
                Collectors.groupingBy(
                        re -> (Integer) re[0],
                        Collectors.toMap(
                                re -> (Integer) re[1],
                                re -> (Long) re[2]
                        )
                )
        );
    }

    /**
     * 查询出Exchange各个requirementType类型数量，根据数量排序
     */
    public Map<Integer, Long> findRequirementTypeCounts() {
        return repository.findRequirementTypeCounts().stream().collect(
                Collectors.toMap(
                        re -> (Integer) re[0],
                        re -> (Long) re[1]
                )
        );
    }
    /**
     * 查询出Exchange的progress属性值为1的个数
     */
    public int countByProgress(int progress) {
        return repository.countByProgress(progress);
    }

}
