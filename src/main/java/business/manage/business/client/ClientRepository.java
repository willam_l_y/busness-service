package business.manage.business.client;

import business.manage.business.client.Client;
import business.manage.business.project.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ClientRepository extends JpaRepository<Client,Integer> {
    @Query("SELECT c.type, COUNT(c) FROM Client c GROUP BY c.type")
    List<Object[]> findClientTypeCounts();
    Page<Client> findAll(Pageable pageable);

    @Query("SELECT c FROM Client c JOIN c.abilities a WHERE a.id = :abilityId")
    List<Client> findByAbilityId(Integer abilityId);

    @Query("SELECT c FROM Client c JOIN c.demands d WHERE d.id = :demandId")
    List<Client> findByDemandId(Integer demandId);


    /**
     * 查询出制定客户,根据交流列表中的需求/对接类别筛选
     */
    @Query("SELECT distinct p FROM Project p " +
            "JOIN p.clientDockingPeople cdp " +
            "JOIN cdp.clients c " +
            "JOIN p.mileposts mp " +
            "JOIN mp.exchanges e " +
            "WHERE c.id = :clientId AND e.requirementType = :requirementType")
    List<Project> findByClientIdAndRequirementType(int clientId, int requirementType);

    /**
     * 查询指定客户的对接类别
     * @param clientId
     * @return
     */
    @Query("SELECT DISTINCT e.requirementType FROM Exchange e " +
            "JOIN e.milepost mp " +
            "JOIN mp.project p " +
            "JOIN p.clientDockingPeople cdp " +
            "JOIN cdp.clients c " +
            "WHERE c.id = :clientId")
    List<Integer> findRequirementTypesByClientId(@Param("clientId") int clientId);
}
