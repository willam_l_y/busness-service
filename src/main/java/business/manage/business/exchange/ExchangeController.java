package business.manage.business.exchange;


import business.manage.business.common.CommonResponse;
import business.manage.business.milepost.Milepost;
import business.manage.business.milepost.MilepostRepository;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static business.manage.business.common.Tools.genCommonResponseResponseEntity;

@RestController
@RequestMapping("/businessManage")
@Api(value = "交流详情")
@CrossOrigin
public class ExchangeController {
    private final ExchangeRepository exchangeRepository;
    private final MilepostRepository milepostRepository;

    public ExchangeController(ExchangeRepository exchangeRepository, MilepostRepository milepostRepository) {
        this.exchangeRepository = exchangeRepository;
        this.milepostRepository = milepostRepository;
    }

    @GetMapping("/exchanges")
    public ResponseEntity<CommonResponse<Page<Exchange>>> all(Pageable pageable) {
        return genCommonResponseResponseEntity(exchangeRepository.findAll(pageable));
    }
    @GetMapping("/exchanges/{id}")
    public ResponseEntity<CommonResponse<Exchange>> one(@PathVariable(name = "id") int id) {
        return genCommonResponseResponseEntity(exchangeRepository.findById(id).orElseThrow());
    }

    /**
     * 通过客户id查找交流详情
     * @param clientId
     * @return
     */
    @GetMapping("/exchanges/findByClientId/{id}")
    public HttpEntity<CommonResponse<List<ExchangeWithProjectName>>> findExchangesByClientId(@PathVariable(name = "id") int clientId) {
        List<Object[]> results = exchangeRepository.findExchangesAndProjectNameByClientId(clientId);
        List<ExchangeWithProjectName> exchangeWithProjectNames = results.stream()
                .map(result -> new ExchangeWithProjectName((Exchange) result[0], (String) result[1]))
                .collect(Collectors.toList());
        return genCommonResponseResponseEntity(exchangeWithProjectNames);
    }

    /**
     * 通过里程碑id查找交流详情
     * @param milepostId
     * @return
     */
    @GetMapping("/exchanges/findByMilepostId/{id}")
    public ResponseEntity<CommonResponse<List<Exchange>>>findExchangesByMilepostId(@PathVariable(name = "id") int milepostId) {
        return genCommonResponseResponseEntity(exchangeRepository.findExchangesByMilepostId(milepostId));
    }
}
