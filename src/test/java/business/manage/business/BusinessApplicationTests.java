package business.manage.business;

import business.manage.business.client.Client;
import business.manage.business.client.ClientRepository;
import business.manage.business.exchange.ExchangeRepository;
import business.manage.business.milepost.Milepost;
import business.manage.business.milepost.MilepostRepository;
import business.manage.business.project.Project;
import business.manage.business.project.ProjectRepository;
import business.manage.business.project.ProjectService;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static business.manage.business.Tools.comDateParser;

//@SpringBootTest
class BusinessApplicationTests {
    private static final Logger log = LoggerFactory.getLogger(BusinessApplicationTests.class);
//    @Autowired
    private ProjectRepository projectRepository;
//    @Autowired
    private MilepostRepository milepostRepository;
//    @Autowired
    private ClientRepository clientRepository;
//    @Autowired
    private ExchangeRepository exchangeRepository;
//    @Autowired
    private ProjectService projectService;

    //    @BeforeEach
    void contextLoads() {
        Client client = new Client();
        client.setName("clientName");
        client.setType(1);
        client.setCity("clientCity");
        client.setArea("clientArea");
        clientRepository.save(client);
        Project project = new Project();
        project.setName("projectName");
        project.setType(1);
        project.setIconUrl("projectIconUrl");
//        project.setClient(client);
        projectRepository.save(project);
        var res = projectRepository.save(project);
        System.out.println("res");
        //创建 Exchange对象，并保存到Milepost中
//		Exchange exchange = new Exchange();
//		exchange.setFillInDate("2024-04-30");
//		exchange.setOurPersonnelOutputContent("ourPersonnelOutputContent");
//		exchange.setCustomerFeedbackContent("customerFeedbackContent");
//		exchange.setOffSiteInteractions("offSiteInteractions");
//		exchange.setDifficultyPoints("difficultyPoints");
//		exchange.setCooperationPoints("cooperationPoints");
//		exchange.setOpportunityPoints("opportunityPoints");
//		exchange.setProgramContent("programContent");
//		exchange.setDepartmentalSupport("departmentalSupport");
//		exchange.setRequirementCategory(1);
//		exchange.setProgress(1);
//		exchange.setCustomerRatings("customerRatings");
//		exchange.setCooperationIntentions("cooperationIntentions");
//		exchange.setOrderAmount("orderAmount");
//		exchange.setSigningDate("signingDate");
//		exchange.setFk_milepost_id(milepost.getId());
//		exchangeRepository.save(exchange);


    }


    void getNow() {
        log.warn("{测试时间工具类}", System.currentTimeMillis());
        var inThisWeek = Tools.isTimeInThisWeek(0L);
        log.warn("{是否在本周}" + inThisWeek, inThisWeek);
        //Tools.localDateToLong( LocalDateTime.now());
    }


    //测试ProjectController的search方法
//    @Test
    @Transactional
    void testSearch() {
        System.out.println("hello");

        log.error(">>>>>>>>>>>>>>>>>>>>>>>>");
        System.out.println("A:" + projectService.getFirstDayOfMonth("2024-11"));
        log.error("++++++++++++++++++++++++++");
//        res.forEach(System.out::println);
        log.error(">>>>>>>>>>>>>>>>>>>>>>>>");
    }

}
