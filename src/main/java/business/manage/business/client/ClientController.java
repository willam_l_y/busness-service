package business.manage.business.client;

import business.manage.business.ability.Ability;
import business.manage.business.ability.AbilityRepository;
import business.manage.business.common.CommonResponse;
import business.manage.business.demand.Demand;
import business.manage.business.demand.DemandRepository;
import business.manage.business.docking_people.ClientDockingPeople;
import business.manage.business.docking_people.ClientDockingPeopleRepository;
import business.manage.business.project.Project;
import io.swagger.annotations.Api;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static business.manage.business.common.Tools.genCommonResponseResponseEntity;

@RestController
@RequestMapping("/businessManage")
@Api(value = "客户")
@CrossOrigin
public class ClientController {
    private final ClientRepository clientRepository;
    private final ClientDockingPeopleRepository clientDockingPeopleRepository;
    private final AbilityRepository abilityRepository;
    private final DemandRepository demandRepository;

    public ClientController(ClientRepository clientRepository, ClientDockingPeopleRepository clientDockingPeopleRepository, AbilityRepository abilityRepository, DemandRepository demandRepository) {
        this.clientRepository = clientRepository;
        this.clientDockingPeopleRepository = clientDockingPeopleRepository;
        this.abilityRepository = abilityRepository;
        this.demandRepository = demandRepository;
    }

    @PostMapping("/clients")
    @Transactional
    public ResponseEntity<CommonResponse<Client>> create(@Valid @RequestBody Client client) {
        if (client.getClientDockingPeople() != null) {
            client.setClientDockingPeople(client.getClientDockingPeople().stream().map(
                    clientDockingPeople -> {
                        ClientDockingPeople existingClientDockingPeople =
                                clientDockingPeopleRepository.findById(clientDockingPeople.getId()).orElseThrow();
                        existingClientDockingPeople.getClients().add(client);
                        return existingClientDockingPeople;
                    }
            ).collect(Collectors.toSet()));
        }
        if (client.getAbilities() != null) {
            client.setAbilities(client.getAbilities().stream().map(
                    ability -> abilityRepository.findById(ability.getId()).orElseThrow(
                            () -> new ResourceNotFoundException("Ability not found with id " + ability.getId()))).collect(Collectors.toSet()));
        }
        if (client.getDemands() != null) {
            client.setDemands(client.getDemands().stream().map(
                    demand -> demandRepository.findById(demand.getId()).orElseThrow(
                            () -> new ResourceNotFoundException("Demand not found with id " + demand.getId()))).collect(Collectors.toSet()));
        }
        return genCommonResponseResponseEntity(clientRepository.save(client));
    }

    @GetMapping("/clients")
    public ResponseEntity<CommonResponse<Page<Client>>> all(Pageable pageable) {
        return genCommonResponseResponseEntity(clientRepository.findAll(pageable));
    }

    @GetMapping("/clients/{id}")
    public ResponseEntity<CommonResponse<Client>> one(@PathVariable(name = "id") int id) {
        return genCommonResponseResponseEntity(clientRepository.findById(id).orElseThrow());
    }

    @PatchMapping("/clients")
    public ResponseEntity<CommonResponse<Client>> update(@RequestBody Client reqClient) {
        return clientRepository.findById(reqClient.id).map(
                client -> {
                    if (reqClient.getClientDockingPeople() != null) {
                        client.getClientDockingPeople().clear();
                        var exitingDockingPeople = reqClient.getClientDockingPeople().stream().map(
                                clientDockingPeople -> {
                                    ClientDockingPeople existingClientDockingPeople =
                                            clientDockingPeopleRepository.findById(clientDockingPeople.getId()).orElseThrow();
                                    existingClientDockingPeople.getClients().clear();
                                    return existingClientDockingPeople;
                                }
                        ).collect(Collectors.toSet());
                        client.setClientDockingPeople(exitingDockingPeople);
                    }
                    if (reqClient.getName() != null) {
                        client.setName(reqClient.getName());
                    }
                    if (reqClient.getGroupsName() != null) {
                        client.setGroupsName(reqClient.getGroupsName());
                    }
                    if (reqClient.getType() != null) {
                        client.setType(reqClient.getType());
                    }
                    if (reqClient.getCity() != null) {
                        client.setCity(reqClient.getCity());
                    }
                    if (reqClient.getArea() != null) {
                        client.setArea(reqClient.getArea());
                    }
                    if (reqClient.getAbilities() != null) {
                        client.clearAbilities();
                        for (Ability ability : reqClient.getAbilities()) {
                            var a = abilityRepository.findById(ability.getId()).orElseThrow(
                                    () -> new ResourceNotFoundException("Ability not found with id " + ability.getId()));
                            client.addAbility(a);
                        }
                    }
                    if (reqClient.getDemands() != null) {
                        client.clearDemands();
                        for (Demand demand : reqClient.getDemands()) {
                            var d = demandRepository.findById(demand.getId()).orElseThrow(
                                    () -> new ResourceNotFoundException("Demand not found with id " + demand.getId()));
                            client.addDemand(d);
                        }
                    }
                    if (reqClient.getIntroduction() != null) {
                        client.setIntroduction(reqClient.getIntroduction());
                    }
                    if (reqClient.getAbilities() != null) {
                        client.getAbilities().clear(); // 清空现有的abilities集合
                        client.getAbilities().addAll(reqClient.getAbilities()); // 添加新的abilities
                    }
                    if (reqClient.getDemands() != null) {
                        client.getDemands().clear(); // 清空现有的demands集合
                        client.getDemands().addAll(reqClient.getDemands()); // 添加新的demands
                    }
                    System.out.println(client);
                    return genCommonResponseResponseEntity(clientRepository.save(client));
                }
        ).orElseThrow(() -> new ResourceNotFoundException("Client not found with id " + reqClient.id));
    }

    @GetMapping("/clients/filterByAbility")
    public ResponseEntity<CommonResponse<List<Client>>> filterClientsByAbility(@RequestParam Integer abilityId) {
        List<Client> filteredClients = clientRepository.findByAbilityId(abilityId);
        return genCommonResponseResponseEntity(filteredClients);
    }

    @GetMapping("/clients/filterByDemand")
    public ResponseEntity<CommonResponse<List<Client>>> filterClientsByDemand(@RequestParam Integer demandId) {
        List<Client> filteredClients = clientRepository.findByDemandId(demandId);
        return genCommonResponseResponseEntity(filteredClients);
    }

    /**
     * 根据客户id和需求类型筛选项目
     *
     * @param clientId
     * @param requirementType
     * @return
     */
    @GetMapping("/clients/filterByClientIdAndRequirementType")
    public ResponseEntity<CommonResponse<List<Project>>> filterByClientIdAndRequirementType(@RequestParam int clientId, @RequestParam int requirementType) {
        List<Project> projects = clientRepository.findByClientIdAndRequirementType(clientId, requirementType);
        return genCommonResponseResponseEntity(projects);
    }

    /**
     * 查询指定客户的对接类别
     * @param clientId 客户id
     * @return
     */
    @GetMapping("/clients/{clientId}/requirementType")
    public ResponseEntity<CommonResponse<List<Integer>>> findRequirementTypeByClientId(@PathVariable(name = "clientId") int clientId) {
        return genCommonResponseResponseEntity(clientRepository.findRequirementTypesByClientId(clientId));
    }
}
