package business.manage.business.work_summary;

import business.manage.business.common.CommonResponse;
import business.manage.business.milepost.Milepost;
import business.manage.business.milepost.MilepostRepository;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static business.manage.business.common.Tools.genCommonResponseResponseEntity;

@RestController
@RequestMapping("/businessManage")
@Api(value = "工作总结")
@CrossOrigin
public class WorkSummaryController {
    private final WorkSummaryRepository workSummaryRepository;
    private final MilepostRepository milepostRepository;


    public WorkSummaryController(WorkSummaryRepository workSummaryRepository, MilepostRepository milepostRepository) {
        this.workSummaryRepository = workSummaryRepository;
        this.milepostRepository = milepostRepository;
    }

    @GetMapping("workSummaries")
    public ResponseEntity<CommonResponse<Iterable<WorkSummary>>> all() {
        return genCommonResponseResponseEntity(workSummaryRepository.findAll());
    }
}
