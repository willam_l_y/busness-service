package business.manage.business.demand;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface DemandRepository extends JpaRepository<Demand,Integer> {
    // 添加逻辑删除的方法
    @Transactional
    @Modifying
    @Query("UPDATE Demand d SET d.isDeleted = true WHERE d.id = :id")
    void logicalDelete(@Param("id") Integer id);

    // 修改查询方法，使其只返回未被逻辑删除的记录
    @Query("SELECT d FROM Demand d WHERE d.isDeleted = false")
    List<Demand> findAllNotDeleted();
}
