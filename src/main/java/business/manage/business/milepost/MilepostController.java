package business.manage.business.milepost;

import business.manage.business.common.CommonResponse;
import business.manage.business.exchange.Exchange;
import business.manage.business.issue.Issue;
import business.manage.business.project.ProjectRepository;
import business.manage.business.work_plan.WorkPlan;
import business.manage.business.work_plan.WorkPlanRepository;
import business.manage.business.work_summary.WorkSummary;
import business.manage.business.work_summary.WorkSummaryRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.Comparator;
import java.util.List;
import static business.manage.business.common.Tools.genCommonResponseResponseEntity;

@RestController
@RequestMapping("/businessManage")
@Api(value = "里程碑")
@CrossOrigin
public class MilepostController {
    private final MilepostRepository milepostRepository;
    private final ProjectRepository projectRepository;
    private final WorkPlanRepository workPlanRepository;
    private final WorkSummaryRepository workSummaryRepository;

    public MilepostController(MilepostRepository milepostRepository,
                              ProjectRepository projectRepository,
                              WorkPlanRepository workPlanRepository,
                              WorkSummaryRepository workSummaryRepository
    ) {
        this.milepostRepository = milepostRepository;
        this.projectRepository = projectRepository;
        this.workPlanRepository = workPlanRepository;
        this.workSummaryRepository = workSummaryRepository;
    }

    //    @ApiOperation(value = "给指定的里程碑添加工作计划,默认创建时间为当前时间")
    @PostMapping("/mileposts/{id}/workPlans")
    public ResponseEntity<CommonResponse<Milepost>> addWorkPlanByMilepostId(@PathVariable(name = "id") int milepostId, @RequestBody WorkPlan workPlan) {
        workPlan.setCreateTime(System.currentTimeMillis());
        workPlan.setUpdateTime(System.currentTimeMillis());
        Milepost milepost = milepostRepository.findById(milepostId).orElseThrow();
        workPlan.setMilepost(milepost);
        milepost.getWorkPlans().add(workPlan);
        return genCommonResponseResponseEntity(milepostRepository.save(milepost));
    }

    @PatchMapping("/mileposts/workPlans/{workPlanId}")
    public ResponseEntity<CommonResponse<Milepost>> updateWorkPlanByMilepostId(@PathVariable(name = "workPlanId") int workPlanId, @RequestBody WorkPlan workPlan) {
        WorkPlan oldWorkPlan = workPlanRepository.findById(workPlanId).orElseThrow();
        oldWorkPlan.setUpdateTime(System.currentTimeMillis());
        oldWorkPlan.setWorkPlanContent(workPlan.getWorkPlanContent());
        return genCommonResponseResponseEntity(milepostRepository.save(oldWorkPlan.getMilepost()));
    }

    @PatchMapping("/mileposts/workSummaries/{workSummaryId}")
    public ResponseEntity<CommonResponse<Milepost>> updateWorkSummaryByMilepostId(@PathVariable(name = "workSummaryId") int workSummaryId, @RequestBody WorkSummary workSummary) {
        WorkSummary oldWorkSummary = workSummaryRepository.findById(workSummaryId).orElseThrow();
        oldWorkSummary.setUpdateTime(System.currentTimeMillis());
        oldWorkSummary.setWorkSummaryContent(workSummary.getWorkSummaryContent());
        return genCommonResponseResponseEntity(milepostRepository.save(oldWorkSummary.getMilepost()));
    }

    @GetMapping("/mileposts/{id}/workPlans")
    public ResponseEntity<CommonResponse<Iterable<WorkPlan>>> getWorkPlansByMilepostId(@PathVariable(name = "id") int milepostId) {
        Milepost milepost = milepostRepository.findById(milepostId).orElseThrow();
        List<WorkPlan> sortedWorkPlans = milepost.getWorkPlans().stream().sorted(Comparator.comparing(WorkPlan::getCreateTime).reversed()).toList();
        return genCommonResponseResponseEntity(sortedWorkPlans);
    }

    @GetMapping("/mileposts/{id}/workSummaries")
    public ResponseEntity<CommonResponse<Iterable<WorkSummary>>> getWorkSummariesByMilepostId(@PathVariable(name = "id") int milepostId) {
        Milepost milepost = milepostRepository.findById(milepostId).orElseThrow();
        List<WorkSummary> sortedWorkSummaries = milepost.getWorkSummaries().stream().sorted(Comparator.comparing(WorkSummary::getCreateTime).reversed()).toList();
        return genCommonResponseResponseEntity(sortedWorkSummaries);
    }

    //    @ApiOperation()
    @PostMapping("/mileposts/{id}/workSummaries")
    public ResponseEntity<CommonResponse<Milepost>> addWorkSummaryByMilepostId(@PathVariable(name = "id") int id, @RequestBody WorkSummary workSummary) {
        if (workSummary.getCreateTime() == null) {
            workSummary.setCreateTime(System.currentTimeMillis());
        }
        Milepost milepost = milepostRepository.findById(id).orElseThrow();
        workSummary.setMilepost(milepost);
        milepost.getWorkSummaries().add(workSummary);
        return genCommonResponseResponseEntity(milepostRepository.save(milepost));
    }

    @ApiOperation(value = "给指定的里程碑添加交流详情")
    /**
     * 给指定的里程碑添加交流详情,需要传入里程碑Id
     * @return 里程碑
     */
    @PostMapping("/mileposts/{id}/communication")
    public ResponseEntity<CommonResponse<Milepost>> addCommunicationDetailByMilepostId(@PathVariable(name = "id") int milepostId, @Valid @RequestBody Exchange exchange) {
        Milepost milepost = milepostRepository.findById(milepostId).orElseThrow();
        exchange.setMilepost(milepost);
        milepost.getExchanges().add(exchange);
        return genCommonResponseResponseEntity(milepostRepository.save(milepost));
    }

    /**
     * 给指定的里程碑添加问题
     *
     * @return 问题
     */
    @PostMapping("/mileposts/{id}/issues")
    public ResponseEntity<CommonResponse<Issue>> addIssueByMilepostId(@PathVariable(name = "id") int milepostId, @RequestBody Issue issue) {
        issue.setDate(System.currentTimeMillis());
        Milepost milepost = milepostRepository.findById(milepostId).orElseThrow();
        issue.setMilepost(milepost);
        milepost.getIssues().add(issue);
        var savedMilepost = milepostRepository.save(milepost);
        Issue savedIssue = savedMilepost.getIssues().stream().max(Comparator.comparing(Issue::getId)).orElseThrow();
        return genCommonResponseResponseEntity(savedIssue);
    }


    @GetMapping("/mileposts")
    public ResponseEntity<CommonResponse<Page<Milepost>>> all(Pageable pageable) {
        return genCommonResponseResponseEntity(milepostRepository.findAll(pageable));
    }

    /**
     * 返回的问题是倒叙排列的
     * @param id
     * @return
     */
    @GetMapping("/mileposts/{id}")
    public ResponseEntity<CommonResponse<Milepost>> one(@PathVariable(name = "id") int id) {
        Milepost milepost = milepostRepository.findById(id).orElseThrow();
        List<Issue> issues = milepost.getIssues();
        issues.sort(Comparator.comparing(Issue::getDate).reversed());
        List<WorkPlan> workPlans = milepost.getWorkPlans();
        workPlans.sort(Comparator.comparing(WorkPlan::getCreateTime).reversed());
        List<WorkSummary> workSummaries = milepost.getWorkSummaries();
        workSummaries.sort(Comparator.comparing(WorkSummary::getCreateTime).reversed());
        return genCommonResponseResponseEntity(milepost);
    }
}
