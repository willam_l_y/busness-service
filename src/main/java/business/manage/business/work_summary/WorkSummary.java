package business.manage.business.work_summary;

import business.manage.business.milepost.Milepost;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jdk.jfr.Enabled;
import lombok.Data;

import javax.persistence.*;

/**
 * 工作总结

 */
@Data
@Entity
@ApiModel(description = "工作总结")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class WorkSummary {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ApiModelProperty("工作总结内容")
    private String workSummaryContent;
    @ApiModelProperty("里程碑")
    @ManyToOne
    @JsonIgnore
    private Milepost milepost;
    @ApiModelProperty("创建时间")
    private Long createTime;
    @ApiModelProperty("更新时间")
    private Long updateTime;
}
