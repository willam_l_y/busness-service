package business.manage.business.work_summary;

import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkSummaryRepository extends JpaRepository<WorkSummary,Integer> {

}
