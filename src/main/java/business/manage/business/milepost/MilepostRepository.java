package business.manage.business.milepost;

import business.manage.business.milepost.Milepost;
import org.springframework.data.jpa.repository.JpaRepository;


public interface MilepostRepository extends JpaRepository<Milepost, Integer> {
    // 通过里程碑ID找到所属的项目
//    Project findByMilepostId(Integer id);
}
