package business.manage.business.project;

import business.manage.business.common.CommonResponse;
import business.manage.business.docking_people.ClientDockingPeople;
import business.manage.business.docking_people.ClientDockingPeopleRepository;
import business.manage.business.milepost.Milepost;
import business.manage.business.milepost.MilepostRepository;
import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.annotations.Api;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static business.manage.business.common.Tools.genCommonResponseResponseEntity;

@RestController
@Api(value = "项目")
@RequestMapping("/businessManage")
@CrossOrigin
public class ProjectController {
    private final ProjectRepository projectRepository;
    private final MilepostRepository milepostRepository;
    private final ClientDockingPeopleRepository clientDockingPeopleRepository;
    private final ProjectService service;

    public ProjectController(
            ProjectRepository projectRepository,
            MilepostRepository milepostRepository,
            ClientDockingPeopleRepository clientDockingPeopleRepository,
            ProjectService service) {
        this.projectRepository = projectRepository;
        this.milepostRepository = milepostRepository;
        this.clientDockingPeopleRepository = clientDockingPeopleRepository;
        this.service = service;
    }

    @GetMapping("/projects/{id}")
    public Project one(@PathVariable(name = "id") int id) {
        return projectRepository.findById(id).orElseThrow();
    }

    @GetMapping("/projects")
    public ResponseEntity<CommonResponse<Page<Project>>> all(Pageable pageable) {
        return genCommonResponseResponseEntity(projectRepository.findAll(pageable));
    }

    @PostMapping("/projects/{id}/mileposts")
    public Project addMilepostByProjectId(@PathVariable(name = "id") int projectId, @Valid @RequestBody List<Milepost> mileposts) {
        var project = projectRepository.findById(projectId).orElseThrow();
        for (var milepost : mileposts) {
            milepost.setProject(project);
            // 里程碑的创建时间为当前时间
            LocalDate localDate = LocalDate.now();
            milepost.setCreateDate(localDate);
            project.getMileposts().add(milepost);
        }
        return projectRepository.save(project);
    }

    @PatchMapping("/projects")
    public Project update(@RequestBody Project reqProject) {
        return projectRepository.findById(reqProject.getId()).map(
                project1 -> {
                    if (reqProject.getName() != null) {
                        project1.setName(reqProject.getName());
                    }
                    if (reqProject.getType() != null) {
                        project1.setType(reqProject.getType());
                    }
                    if (reqProject.getIconUrl() != null) {
                        project1.setIconUrl(reqProject.getIconUrl());
                    }
                    if (reqProject.getPrincipal() != null) {
                        project1.setPrincipal(reqProject.getPrincipal());
                    }
                    if (reqProject.getTarget() != null) {
                        project1.setTarget(reqProject.getTarget());
                    }
                    if (reqProject.getCurrentMilepost() != null) {
                        project1.setCurrentMilepost(reqProject.getCurrentMilepost());
                    }
                    
                    if (reqProject.getClientDockingPeople() != null) {
                        project1.setClientDockingPeople(reqProject.getClientDockingPeople());
                    }
                    if (reqProject.getMileposts() != null) {
                        project1.setMileposts(reqProject.getMileposts());
                    }

//                    if (reqProject.getOurDockingPeople() != null) {
//                        project1.setOurDockingPeople(reqProject.getOurDockingPeople());
//                    }
//                    if (reqProject.getOurGroupOfDockingPeople() != null) {
//                        project1.setOurGroupOfDockingPeople(reqProject.getOurGroupOfDockingPeople());
//                    }
//                    if (reqProject.getProgress() != null) {
//                        project1.setProgress(reqProject.getProgress());
//                    }
                    return projectRepository.save(project1);
                }
        ).orElseThrow(() -> new RuntimeException("Project not found with id " + reqProject.getId())
        );
    }
    //
    //项目类型是可选的，月份是必选的
//    @ApiOperation(value = "根据项目类型,月份筛选项目列表；月份的格式为：yyyy-MM")

    /**
     * 项目的类型与里程碑（Mileposts）的月份对项目列表筛选，无分页有功能(月份的格式为：yyyy-MM")
     * @param month
     * @param type
     * @return
     */
    @GetMapping("/projects/search")
    @JsonView(Views.List.class)
    public List<Project> search(
            @RequestParam(name = "month") String month,
            @RequestParam(name = "type", required = false) Integer type
    ) {
        return service.findProjectsByMilepostsBetweenAndType(month, Optional.ofNullable(type));
    }

    @PostMapping("/projects")
    public ResponseEntity<CommonResponse<Project>> create(@Valid @RequestBody Project project) {
        var dockingList = project.getClientDockingPeopleIds();
        if (dockingList == null) {
            dockingList = Collections.emptyList();
        }
        var DockingPeopleFromDB = dockingList.stream().map(id -> {
            ClientDockingPeople clientDockingPeople = clientDockingPeopleRepository.findById(id).orElseThrow();
            clientDockingPeople.setProjects(project);
            return clientDockingPeople;
        }).collect(Collectors.toList());
        project.setClientDockingPeople(DockingPeopleFromDB);
        return genCommonResponseResponseEntity(projectRepository.save(project));
    }
}
