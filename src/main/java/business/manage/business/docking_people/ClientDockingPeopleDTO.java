package business.manage.business.docking_people;

import business.manage.business.client.Client;
import business.manage.business.project.Project;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 客户对接人DTO
 * 客户对接人实体做为与客户多对多关系的子实体:属性中没有客户信息
 * 使用改DTO添加额外的客户信息属性
 */
@Getter
@Setter
public class ClientDockingPeopleDTO {
    private Integer id;
    private String name;
    private String departmentLevel1;
    private String departmentLevel2;
    private String position;
    private int age;
    private Integer gender;
    private String nativePlace;
    private String address;
    private String graduateSchool;
    private String childrenSituation;
    private String maritalStatus;
    private String workExperience;
    private String relationship;
    private String phone;
    private String mail;
    private Set<ClientNameAndId> clients;
    public ClientDockingPeopleDTO(){}
    public ClientDockingPeopleDTO(
            ClientDockingPeople clientDockingPeople
    ) {
        this.id = clientDockingPeople.getId();
        this.name = clientDockingPeople.getName();
        this.departmentLevel1 = clientDockingPeople.getDepartmentLevel1();
        this.departmentLevel2 = clientDockingPeople.getDepartmentLevel2();
        this.position = clientDockingPeople.getPosition();
        this.age = clientDockingPeople.getAge();
        this.gender = clientDockingPeople.getGender();
        this.nativePlace = clientDockingPeople.getNativePlace();
        this.address = clientDockingPeople.getAddress();
        this.graduateSchool = clientDockingPeople.getGraduateSchool();
        this.childrenSituation = clientDockingPeople.getChildrenSituation();
        this.maritalStatus = clientDockingPeople.getMaritalStatus();
        this.workExperience = clientDockingPeople.getWorkExperience();
        this.relationship = clientDockingPeople.getRelationship();
        this.phone = clientDockingPeople.getPhone();
        this.mail = clientDockingPeople.getMail();
        var outedClients = clientDockingPeople.getClients().stream().map(
                client -> new ClientNameAndId(client.getId(), client.getName())
        ).collect(Collectors.toSet());
        this.clients = outedClients;
    }
}

@Getter
@Setter
class ClientNameAndId {
    Integer id;
    String name;

    public ClientNameAndId(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
}
