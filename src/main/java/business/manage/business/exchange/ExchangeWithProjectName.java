package business.manage.business.exchange;
public class ExchangeWithProjectName {
    private Exchange exchange;
    private String projectName;

    // Constructor, getters, and setters
    public ExchangeWithProjectName(Exchange exchange, String projectName) {
        this.exchange = exchange;
        this.projectName = projectName;
    }

    // Getters and setters
    public Exchange getExchange() {
        return exchange;
    }

    public void setExchange(Exchange exchange) {
        this.exchange = exchange;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}