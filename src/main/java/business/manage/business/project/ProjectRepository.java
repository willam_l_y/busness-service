package business.manage.business.project;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface ProjectRepository extends JpaRepository<Project, Integer> {

    //多条件查询：
    // 找到Mileposts的最早的createDate时间在指定的月份之中的项目
    @Query("SELECT p FROM Project p JOIN p.mileposts m WHERE m.createDate >= :start AND m.createDate < :end AND (:type is null or p.type = :type) GROUP BY p")
    List<Project> findProjectsFiltrateByMilepostsMonthAndType(
            @Param("start") LocalDate start,
            @Param("end") LocalDate end,
//            @Param("clientId") int clientId,
            @Param("type")Optional<Integer> type
    );
}
