package business.manage.business.milepost;

import business.manage.business.exchange.Exchange;
import business.manage.business.issue.Issue;
import business.manage.business.project.Project;
import business.manage.business.work_plan.WorkPlan;
import business.manage.business.work_summary.WorkSummary;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

/**
 * 里程碑
 */
@Data
@Entity
@AllArgsConstructor
@ApiModel(description = "里程碑")
@NoArgsConstructor
@ToString(exclude = {"project","exchanges","workPlans","workSummaries","issues"})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Milepost {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer milepostId;

    @ApiModelProperty("创建日期；格式：yyyy-MM-dd")
    private LocalDate createDate;

    @ApiModelProperty("名称")
    @NotNull
    private String name;

    @ApiModelProperty("负责人")
    @NotNull
    private String principal;

    @NotNull
    @ApiModelProperty("描述")
    private String describeContent;


    @ApiModelProperty("项目")
    @ManyToOne
    @JsonBackReference
    private Project project;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "milepost")
    @ApiModelProperty("交流详情")
    @JsonIgnore
    private List<Exchange> exchanges;

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "milepost")
    @ApiModelProperty("工作计划")
    private List<WorkPlan> workPlans;

    @ApiModelProperty("工作总结")
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "milepost")
    private List<WorkSummary> workSummaries;

    @ApiModelProperty("问题")
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "milepost")
    private List<Issue> issues;

}
