package business.manage.business.demand;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/businessManage")
@Api(value = "需求")
@CrossOrigin
public class DemandController {
    private final DemandRepository demandRepository;

    public DemandController(DemandRepository demandRepository) {
        this.demandRepository = demandRepository;
    }

    @GetMapping("/demands")
    public List<Demand> getAllDemands(
            @RequestParam(name = "deleted", defaultValue = "false") boolean isDeleted) {
        if (!isDeleted) {
            return demandRepository.findAll();
        } else {
            return demandRepository.findAllNotDeleted();
        }
    }

    @PostMapping("/demands")
    public Demand create(@RequestBody Demand demand) {
        return demandRepository.save(demand);
    }

    @DeleteMapping("/demands/{id}")
    public void delete(@PathVariable(name = "id") int id) {
        demandRepository.logicalDelete(id);
    }
}
