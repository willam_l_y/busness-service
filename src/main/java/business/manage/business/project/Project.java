package business.manage.business.project;

import business.manage.business.client.Client;
import business.manage.business.docking_people.ClientDockingPeople;
import business.manage.business.milepost.Milepost;
import com.fasterxml.jackson.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.List;

/**
 * 项目
 */
@Data
@Entity
@ToString(exclude = {"mileposts", "clientDockingPeople"})
@ApiModel(description = "项目")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.List.class) // 用于序列化时指定序列化的视图
    private int id;
    @JsonView(Views.List.class)
    private String name;

    @Min(0)
    @Max(3)
    @JsonView(Views.List.class)
    @ApiModelProperty("项目类型（项目合作:0，战略合作:1，会议活动:2，资质荣誉:3）")
    private Integer type;

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "project")
    @ApiModelProperty("里程碑")
    @JsonManagedReference
    private List<Milepost> mileposts;
    @ApiModelProperty("图标地址")
    @JsonView(Views.List.class)
    private String iconUrl;

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "projects")
    @JsonView(Views.List.class)
    @ApiModelProperty("客户对接人")
    private List<ClientDockingPeople> clientDockingPeople;

    /**
     * 用于情况时承载客户对接人的id,不会记录到数据库中
     */
    @Transient
    @JsonView(Views.List.class)
    private List<Integer> clientDockingPeopleIds;

    @ApiModelProperty("我方对接人")
    @JsonView(Views.List.class)
    private String ourDockingPeople;

    @ApiModelProperty("责任人")
    @JsonView(Views.List.class)
    private String principal;

    @ApiModelProperty("目标")
    @JsonView(Views.List.class)
    private String target;

    @ApiModelProperty("当前里程碑")
    @JsonView(Views.List.class)
    private String currentMilepost;

    @ApiModelProperty("当前进度(待接触:0、洽谈:1、报价:2、投标:3、签单:4、暂停:5、终止:6)")
    @Max(6)
    @Min(0)
    @JsonView(Views.List.class)
    private Integer progress;
}
