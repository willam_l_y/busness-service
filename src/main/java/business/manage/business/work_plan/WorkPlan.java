package business.manage.business.work_plan;

import business.manage.business.milepost.Milepost;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

@Data
@Entity
@ApiModel(description = "工作计划")
@ToString(exclude = {"milepost"})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class WorkPlan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ApiModelProperty("内容")
    private String workPlanContent;
    @ApiModelProperty("创建时间")
    private Long createTime;
    @ApiModelProperty("更新时间")
    private Long updateTime;
    @ApiModelProperty("里程碑")
    @JsonIgnore
    @ManyToOne
    private Milepost milepost;
}
