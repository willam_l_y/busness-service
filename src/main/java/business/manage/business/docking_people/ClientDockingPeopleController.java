package business.manage.business.docking_people;

import business.manage.business.client.ClientRepository;
import business.manage.business.common.CommonResponse;
import io.swagger.annotations.Api;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static business.manage.business.common.Tools.genCommonResponseResponseEntity;

import org.springframework.data.domain.Pageable;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;


@RestController
@RequestMapping("/businessManage")
@Api(value = "客户对接人")
@CrossOrigin
public class ClientDockingPeopleController {
    private final ClientDockingPeopleRepository repository;
    private final ClientRepository clientRepository;

    public ClientDockingPeopleController(
            ClientDockingPeopleRepository repository,
            ClientRepository clientRepository) {
        this.repository = repository;
        this.clientRepository = clientRepository;
    }

    /**
     * 添加对接人
     *
     * @param dockingPeople
     * @return
     */
    @PostMapping("/clientDocking")
    public ResponseEntity<CommonResponse<ClientDockingPeople>> create(@Valid @RequestBody ClientDockingPeople dockingPeople) {
        return genCommonResponseResponseEntity(repository.save(dockingPeople));
    }

    /**
     * 更新对接人
     */
    @PatchMapping("/clientDocking")
    public ResponseEntity<CommonResponse<ClientDockingPeople>> update(@RequestBody ClientDockingPeopleDTO reqDockingPeopleDTO) {
        var reqDockingPeople = reqDockingPeopleDTO;
        return repository.findById(reqDockingPeople.getId()).map(
                dockingPeople -> {
                    dockingPeople.setName(reqDockingPeople.getName() != null ? reqDockingPeople.getName() : dockingPeople.getName());
                    dockingPeople.setDepartmentLevel1(reqDockingPeople.getDepartmentLevel1() != null ? reqDockingPeople.getDepartmentLevel1() : dockingPeople.getDepartmentLevel1());
                    dockingPeople.setDepartmentLevel2(reqDockingPeople.getDepartmentLevel2() != null ? reqDockingPeople.getDepartmentLevel2() : dockingPeople.getDepartmentLevel2());
                    dockingPeople.setPosition(reqDockingPeople.getPosition() != null ? reqDockingPeople.getPosition() : dockingPeople.getPosition());
                    dockingPeople.setAge(reqDockingPeople.getAge() != 0 ? reqDockingPeople.getAge() : dockingPeople.getAge());
                    dockingPeople.setGender(reqDockingPeople.getGender() != null ? reqDockingPeople.getGender() : dockingPeople.getGender());
                    dockingPeople.setNativePlace(reqDockingPeople.getNativePlace() != null ? reqDockingPeople.getNativePlace() : dockingPeople.getNativePlace());
                    dockingPeople.setAddress(reqDockingPeople.getAddress() != null ? reqDockingPeople.getAddress() : dockingPeople.getAddress());
                    dockingPeople.setGraduateSchool(reqDockingPeople.getGraduateSchool() != null ? reqDockingPeople.getGraduateSchool() : dockingPeople.getGraduateSchool());
                    dockingPeople.setChildrenSituation(reqDockingPeople.getChildrenSituation() != null ? reqDockingPeople.getChildrenSituation() : dockingPeople.getChildrenSituation());
                    dockingPeople.setMaritalStatus(reqDockingPeople.getMaritalStatus() != null ? reqDockingPeople.getMaritalStatus() : dockingPeople.getMaritalStatus());
                    dockingPeople.setWorkExperience(reqDockingPeople.getWorkExperience() != null ? reqDockingPeople.getWorkExperience() : dockingPeople.getWorkExperience());
                    dockingPeople.setRelationship(reqDockingPeople.getRelationship() != null ? reqDockingPeople.getRelationship() : dockingPeople.getRelationship());
                    if (reqDockingPeopleDTO.getClients() != null) {
                        // 获取之前的客户ID
                        var oldId = dockingPeople.getClients().stream().map(
                                client -> client.getId()
                        ).collect(Collectors.toSet());
                        // 获取新的客户id
                        var newId = reqDockingPeopleDTO.getClients().stream().map(
                                clientNameAndId -> clientNameAndId.getId()
                        ).collect(Collectors.toSet());
                        var deleteIdSet = new HashSet<>(oldId);
                        var addIdSet = new HashSet<>(newId);

                        // 获取新的客户id与旧的客户id的差集
                        addIdSet.removeAll(oldId);
                        deleteIdSet.removeAll(newId);
                        addIdSet.forEach(addId -> {
                                    var client = clientRepository.findById(addId).orElseThrow(() -> new ResourceNotFoundException("Client not found with id " + addId));
                                    client.getClientDockingPeople().add(dockingPeople);
//                                    clientRepository.save(client);
                                }
                        );
                        deleteIdSet.forEach(deleteId -> {
                            var client = clientRepository.findById(deleteId).orElseThrow(() -> new ResourceNotFoundException("Client not found with id " + deleteId));
                            client.getClientDockingPeople().remove(dockingPeople);
//                            clientRepository.save(client);
                        });

                    }
                    return genCommonResponseResponseEntity(repository.save(dockingPeople));
                }
        ).orElseThrow(() -> new ResourceNotFoundException("ClientDockingPeople not found with id " + reqDockingPeople.getId()));
    }

    /**
     * 客户对接人列表(可根据姓名搜索)
     *
     * @return 与姓名匹配的对接人
     */

    @GetMapping("/clientDocking")
    public ResponseEntity<CommonResponse<Page<ClientDockingPeopleDTO>>> all(Pageable pageable, @Valid @RequestParam(required = false) String param) {
        Page<ClientDockingPeople> peoplePage;
        if (param == null || param.isEmpty()) {
            peoplePage = repository.findAll(pageable);
        } else {
            peoplePage = repository.findByNameContaining(param, pageable);
        }
        List<ClientDockingPeopleDTO> contentDTO = peoplePage.getContent().stream().map(ClientDockingPeopleDTO::new).collect(Collectors.toList());
        Page<ClientDockingPeopleDTO> res = new PageImpl<>(contentDTO, pageable, peoplePage.getTotalElements());
        return genCommonResponseResponseEntity(res);
    }

    //跟据Id获取对接人详情
    @GetMapping("/clientDocking/{id}")
    public ResponseEntity<CommonResponse<ClientDockingPeople>> one(@PathVariable Integer id) {
        return genCommonResponseResponseEntity(repository.findById(id).orElseThrow(() -> new ResourceNotFoundException("ClientDockingPeople")));
    }
}
