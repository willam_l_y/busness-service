package business.manage.business.work_plan;

import business.manage.business.milepost.Milepost;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkPlanRepository extends JpaRepository<WorkPlan,Integer> {
    // 查找制定的里程碑中最新的工作计划
    //WorkPlan findTopByMilepostOrderByUpdateTimeDesc(Milepost milepost);
}
