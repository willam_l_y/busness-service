package business.manage.business.client;

import business.manage.business.ability.Ability;
import business.manage.business.demand.Demand;
import business.manage.business.docking_people.ClientDockingPeople;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@ApiModel(description = "客户组织（集团，公司）")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    @NotBlank
    @NotNull
    @ApiModelProperty("客户组织名称")
    private String name;
    @ApiModelProperty("所属集团")
    @NotBlank(message = "所属集团是必填项目")
    private String groupsName;

    //校验type必须再0至7之间
    @Min(0)
    @Max(7)
    @NotNull
    @ApiModelProperty("类型(主机厂：0、零部件厂：1、集成商：2、设备商：3、平台组织：4、科研院所：5、高校：6、其他：7）")
    private Integer type;

    @ApiModelProperty("所在城市")
    private String city;
    @ApiModelProperty("具体区域")
    private String area;


//    @NotNull
    @ManyToMany
    @ApiModelProperty("客户对接人")
    private Set<ClientDockingPeople> clientDockingPeople;

    @ManyToMany()
    @ApiModelProperty("客户能力")
    private Set<Ability> abilities = new HashSet<>();

    // 添加能力
    public void addAbility(Ability ability) {
        abilities.add(ability);
        ability.getClients().add(this);
    }
    // 删除能力
    public void removeAbility(Ability ability) {
        abilities.remove(ability);
        ability.getClients().remove(this);
    }

    // 清空能力
    public void clearAbilities() {
        for (Ability ability : abilities) {
            ability.getClients().remove(this);
        }
        abilities.clear();
    }

    @ManyToMany
    @ApiModelProperty("客户需求")
    private Set<Demand> demands;
    /**
     * 添加需求
     */
    public void addDemand(Demand demand) {
        demands.add(demand);
        demand.getClients().add(this);
    }

    /**
     * 删除需求
     */
    public void removeDemand(Demand demand) {
        demands.remove(demand);
        demand.getClients().remove(this);
    }

    /**
     * 清空需求
     */
    public void clearDemands() {
        for (Demand demand : demands) {
            demand.getClients().remove(this);
        }
        demands.clear();
    }




    @ApiModelProperty("客户简介")
    private String introduction;
}

