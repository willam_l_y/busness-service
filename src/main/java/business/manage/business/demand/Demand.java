package business.manage.business.demand;

import business.manage.business.client.Client;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@ApiModel(description = "需求")
public class Demand {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    @ManyToMany()
    @JsonIgnore
    private Set<Client> clients;
    private boolean isDeleted = false;
}