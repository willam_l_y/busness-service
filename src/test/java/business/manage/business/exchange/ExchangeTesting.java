package business.manage.business.exchange;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ExchangeTesting {
    @Autowired
    private ExchangeService service;
    @Autowired
    private ExchangeRepository repository;

    //    @Test
    public void testCountOurGroupOfDockingPeople() {
        //todo 从数据库中查询ourGroupOfDockingPeople属性值为1的个数
        int res0 = service.countOurGroupOfDockingPeople(0);
        int res1 = service.countOurGroupOfDockingPeople(1);
        int res2 = service.countOurGroupOfDockingPeople(2);
        System.out.println(res0);
        System.out.println(res1);
        System.out.println(res2);
        //断言 res 大于等于 1
        assert res0 > 0;
        assert res1 > 0;
        assert res2 > 0;
    }

    //    @Test
//    public void testFindOurGroupOfDockingPeopleCounts() {
//        //todo 从数据库中查询Exchange各个ourGroupOfDockingPeople类型的数量
//        var res = repository.findOurGroupOfDockingPeopleCounts();
//        for (Object[] re : res) {
//            System.out.println(re[0]+"==="+re[1]);
//        }
//        System.out.println("================");
//        assert res.size() > 0;
//    }
    //测试ExchangeService的findOurGroupOfDockingPeopleCountsAndGroupByCooperationIntentions方法
//    @Test
    public void testFindOurGroupOfDockingPeopleCountsAndGroupByCooperationIntentions() {
        var res = service.findOurGroupOfDockingPeopleCountsAndGroupByCooperationIntentions();
        for (var re : res.entrySet()) {
            System.out.println("公司:" + re.getKey());
            for (var r : re.getValue().entrySet()) {
                System.out.println("{意向:" + r.getKey() + ",个数:" + r.getValue() + "}");
            }
        }
        assert res.size() > 0;
    }

    //测试ExchangeRepository的findRequirementTypeCounts方法
//    @Test
    public void testFindRequirementTypeCounts() {
        var res = repository.findRequirementTypeCounts();
        for (var re : res) {
            System.out.println(re[0] + "===" + re[1]);
        }
        assert res.size() > 0;
    }

    //测试ExchangeService的countByProgress方法
//    @Test
//    public void testCountByProgress() {
//        int res = service.countByProgress(1);
//        System.out.println(res);
//        assert res > 0;
//    }
    //测试ExchangeRepository的countByProgress方法
//    @Test
    public void testRepositoryCountByProgress() {
        int res = repository.countByProgress(0);
        System.out.println(res);
        assert res > 0;
    }

    //       @Test
    public void testFindExchangesByClientId() {
        var res = repository.findExchangesAndProjectNameByClientId(1);
        System.out.println("===========>" + res.size());
        for (var re : res) {
            System.out.println("===========>" + re);
        }
        assert res.size() > 0;
    }

    //    @Test
    public void testFindDistinctExchangesByProjectId() {
        var res = repository.findExchangesListByProjectId(1);
        System.out.println("===========>" + res.size());
        for (var re : res) {
            System.out.println("===========>" + re);
        }
        assert res.size() > 0;
    }

}
