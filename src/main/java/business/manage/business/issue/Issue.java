package business.manage.business.issue;

import business.manage.business.comments.Comments;
import business.manage.business.milepost.Milepost;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@ApiModel(description = "问题")
public class Issue {
    public Issue(int id, Long date,String content,String status,String result,List<Comments> comments,Milepost milepost) {
        this.id = id;
        this.date = date;
        this.content = content;
        this.status = status;
        this.result = result;
        this.comments = comments;
        this.milepost = milepost;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    @ApiModelProperty("创建时间")
    Long date;
    @ApiModelProperty("内容")
    String content;
    @ApiModelProperty("状态")
    String status;
    @ApiModelProperty("结果")
    String result;
    @ApiModelProperty("评论")
    @OneToMany
    private List<Comments> comments;
    @ApiModelProperty("里程碑")
    @ManyToOne
    @JsonIgnore
    private Milepost milepost;

    public Issue() {

    }
}