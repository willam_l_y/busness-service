package business.manage.business.exchange;

import business.manage.business.client.Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

public interface ExchangeRepository extends JpaRepository<Exchange, Integer> {
    @GetMapping("/exchanges")
    Page<Exchange> findAll(Pageable pageable);
    /**
     * 我方对接人所属公司（tag）合作意向个数
     * @param tag 所属公司的tag
     * @return 意向的个数
     */
    int countByOurGroupOfDockingPeople(int tag);
    /**
     * 查询出Exchange各个ourGroupOfDockingPeople类型的数量
     */
//    @Query("SELECT e.ourGroupOfDockingPeople, COUNT(e) FROM Exchange e GROUP BY e.ourGroupOfDockingPeople")
//    List<Object[]> findOurGroupOfDockingPeopleCounts();

    /**
     * 查询出Exchange各个ourGroupOfDockingPeople类型的数量并根据cooperationIntentions属性分组,根据ourGroupOfDockingPeople的正序排列
     */
    @Query("SELECT e.ourGroupOfDockingPeople, e.cooperationIntentions, COUNT(e) FROM Exchange e GROUP BY e.ourGroupOfDockingPeople, e.cooperationIntentions ORDER BY e.ourGroupOfDockingPeople ASC")
    List<Object[]> findOurGroupOfDockingPeopleCountsAndGroupByCooperationIntentions();

    /**
     * 查询出Exchange各个requirementType类型数量，根据数量排序
     */
    @Query("SELECT e.requirementType, COUNT(e) FROM Exchange e GROUP BY e.requirementType ORDER BY COUNT(e) DESC")
    List<Object[]> findRequirementTypeCounts();

    /**
     * 查询出Exchange的progress属性值为1的个数
     */
    int countByProgress(int progress);

    /**
     * 查询制定的客户的的Exchange
     * @param clientId
     * @return
     */
    @Query("SELECT DISTINCT e ,p.name FROM Exchange e " +
            "JOIN e.milepost mp " +
            "JOIN mp.project p " +
            "JOIN p.clientDockingPeople cdp " +
            "JOIN cdp.clients c " +
            "WHERE c.id = :clientId")
    List<Object[]> findExchangesAndProjectNameByClientId(@Param("clientId") int clientId);

    /**
     * 查询指定项目的Exchange列表
     * @param projectId
     * @return
     */
    @Query("SELECT DISTINCT e FROM Exchange e " +
            "JOIN e.milepost m " +
            "JOIN m.project p " +
            "WHERE p.id = :projectId")
    List<Exchange> findExchangesListByProjectId(@Param("projectId") int projectId);

    /**
     * 查询指定里程碑的Exchange列表
     * @param milepostId
     * @return 沟通列表
     */
    @Query("SELECT DISTINCT e FROM Exchange e " +
            "JOIN e.milepost m " +
            "WHERE m.milepostId = :milepostId")
    List<Exchange> findExchangesByMilepostId(@Param("milepostId") int milepostId);



}
