package business.manage.business.ability;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/businessManage")
@Api(value = "能力")
@CrossOrigin
public class AbilityController {
    private final AbilityRepository abilityRepository;

    public AbilityController(AbilityRepository abilityRepository) {
        this.abilityRepository = abilityRepository;
    }

    @GetMapping("/abilities")
    public List<Ability> getAllAbilities(
            @RequestParam(name = "containDeleted", defaultValue = "false") boolean containDeleted
    ) {
        if (containDeleted) {
            return abilityRepository.findAll();
        } else {
            return abilityRepository.findAllNotDeleted();
        }
    }

    @PostMapping("/abilities")
    public Ability create(@RequestBody Ability ability) {
        return abilityRepository.save(ability);
    }

    @PatchMapping("/abilities/{id}/delete")
    public void delete(@PathVariable(name = "id") int id) {
        abilityRepository.logicalDelete(id);
    }

    @PatchMapping("/abilities/{id}/undelete")
    public void undelete(@PathVariable(name = "id") int id) {
        abilityRepository.logicalUndelete(id);
    }
}
