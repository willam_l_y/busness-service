package business.manage.business.work_plan;

import business.manage.business.common.CommonResponse;
import business.manage.business.milepost.Milepost;
import business.manage.business.milepost.MilepostRepository;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static business.manage.business.common.Tools.genCommonResponseResponseEntity;

@RestController
@RequestMapping("/businessManage")
@Api(value = "工作计划")
@CrossOrigin
public class WorkPlanController {
    private final WorkPlanRepository workPlanRepository;
    private final MilepostRepository milepostRepository;

    public WorkPlanController(WorkPlanRepository workPlanRepository, MilepostRepository milepostRepository) {
        this.workPlanRepository = workPlanRepository;
        this.milepostRepository = milepostRepository;
    }

    @GetMapping("workPlans")
    public ResponseEntity<CommonResponse<Iterable<WorkPlan>>> all() {
        return genCommonResponseResponseEntity(workPlanRepository.findAll());
    }

}
